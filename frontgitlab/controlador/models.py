from django.db import models

# Create your models here.
from django.conf import settings
from django.utils import timezone


class User(models.Model):
    name = models.CharField(max_length=200)
    email = models.CharField(max_length=200, primary_key=True)
    created_date = models.DateTimeField(default=timezone.now)
    updated_date = models.DateTimeField(blank=True, null=True)
    active = models.BooleanField(default=False)
    def publish(self):
        self.updated_date = timezone.now()
        self.save()

    def __str__(self):
        return self.email


class Project(models.Model):
    project_name = models.CharField(max_length=200, primary_key=True)
    project_url = models.CharField(max_length=200)
    created_date = models.DateTimeField(default=timezone.now)
    updated_date = models.DateTimeField(blank=True, null=True)
    active = models.BooleanField(default=True)
    

    def publish(self):
        self.updated_date = timezone.now()
        self.save()

    def __str__(self):
        return self.project_name



class Branch(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    project_name = models.ForeignKey(Project, on_delete=models.CASCADE)
    branch_name = models.CharField(max_length=200, primary_key=True)
    jira_source = models.CharField(max_length=20)
    tar_0800_source = models.CharField(max_length=20)
    cha_0800_source = models.CharField(max_length=20)
    created_date = models.DateTimeField(default=timezone.now)
    updated_date = models.DateTimeField(blank=True, null=True)
    active = models.BooleanField(default=True)
    

    def publish(self):
        self.updated_date = timezone.now()
        self.save()

    def __str__(self):
        return self.branch_name

