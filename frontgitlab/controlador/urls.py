from django.urls import path
from . import views

urlpatterns = [
    path('', views.project_list, name='project_list'),
    path('project/<str:pk>/',  views.project_detail, name='project_detail'),
    path('project/<str:project_name>/<str:pk>/', views.branch_detail, name='branch_detail'),
]
