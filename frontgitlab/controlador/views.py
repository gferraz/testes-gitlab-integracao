from django.shortcuts import render,get_object_or_404
from .models import *
from django.utils import timezone

# Create your views here.
def project_list(request):
    projects = Project.objects.filter(active=True).order_by('project_name')
    return render(request, 'controlador/project_list.html', {'projects': projects})

def project_detail(request, pk):
    project = get_object_or_404(Project, pk=pk)
    branchs = Branch.objects.filter(active=True,project_name=pk).order_by('branch_name')
    return render(request, 'controlador/project_detail.html',{'project': project, 'branchs':branchs})

def branch_detail(request,project_name, pk):
    project = get_object_or_404(Project, pk=project_name)
    branch = get_object_or_404(Branch, project_name = project_name, pk=pk)
    return render(request,'controlador/branch_detail.html',{'project':project,'branch':branch})